package com.example.demo.controller;

import com.example.demo.dao.NoticeDao;
import com.example.demo.dao.VipDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class VipController {
    @Autowired
    VipDao vipDao;

    @PostMapping("/del_vip")
    public int del_vip(@RequestBody Map map){
        return vipDao.del_vip(map);
    }

    @PostMapping("/add_vip")
    public int add_vip(@RequestBody Map map){
        return vipDao.add_vip(map);
    }

    @PostMapping("/find_all_vip")
    public List<Map> find_all_vip(@RequestBody Map map) throws InterruptedException {

        return vipDao.find_all_vip(map);
    }
}