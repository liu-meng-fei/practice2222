package com.example.demo.controller;
import com.example.demo.controller.NoticeController;
import com.example.demo.controller.GysConller;
import com.example.demo.dao.GysDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class GysConller {
    @Autowired
    GysDao gysDao;

    @PostMapping("/del_gys")
    public int del_gys(@RequestBody Map map){
        return gysDao.del_gys(map);
    }

    @PostMapping("/add_gys")
    public int add_gys(@RequestBody Map map){
        return gysDao.add_gys(map);
    }

    @PostMapping("/find_all_gys")
    public List<Map> find_all_gys(@RequestBody Map map) throws InterruptedException {

        return gysDao.find_all_gys(map);
    }
}
