package com.example.demo.controller;

import com.example.demo.dao.NoticeDao;
import com.example.demo.dao.BwckDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class BwckController {
    @Autowired
    BwckDao bwckDao;

    @PostMapping("/del_bwck")
    public int del_bwck(@RequestBody Map map){
        return bwckDao.del_bwck(map);
    }

    @PostMapping("/add_bwck")
    public int add_bwck(@RequestBody Map map){
        return bwckDao.add_bwck(map);
    }

    @PostMapping("/find_all_bwck")
    public List<Map> find_all_bwck(@RequestBody Map map) throws InterruptedException {

        return bwckDao.find_all_bwck(map);
    }
}
