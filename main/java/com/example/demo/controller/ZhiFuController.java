package com.example.demo.controller;

import com.example.demo.dao.ZhiFuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class ZhiFuController {
    @Autowired
    ZhiFuDao zhiFuDao;

    @PostMapping("/del_zhifu")
    public int del_zhifu(@RequestBody Map map){
        return zhiFuDao.del_zhifu(map);
    }

    @PostMapping("/add_zhifu")
    public int add_zhifu(@RequestBody Map map){
        return zhiFuDao.add_zhifu(map);
    }

    @PostMapping("/find_all_zhifu")
    public List<Map> find_all_zhifu(@RequestBody Map map) throws InterruptedException {

        return zhiFuDao.find_all_zhifu(map);
    }
}
