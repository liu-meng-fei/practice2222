package com.example.demo.controller;

import com.example.demo.dao.NoticeDao;
import com.example.demo.dao.baowaidanDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class baowaidanController {
    @Autowired
    baowaidanDao baowaidanDao;

    @PostMapping("/del_baowaidan")
    public int del_baowaidan(@RequestBody Map map){
        return baowaidanDao.del_baowaidan(map);
    }

    @PostMapping("/add_baowaidan")
    public int add_baowaidan(@RequestBody Map map){
        return baowaidanDao.add_baowaidan(map);
    }

    @PostMapping("/find_all_baowaidan")
    public List<Map> find_all_baowaidan(@RequestBody Map map) throws InterruptedException {

        return baowaidanDao.find_all_baowaidan(map);
    }
}

