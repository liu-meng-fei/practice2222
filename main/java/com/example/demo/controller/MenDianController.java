package com.example.demo.controller;

import com.example.demo.dao.NoticeDao;
import com.example.demo.dao.MenDianDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class MenDianController {
    @Autowired
    MenDianDao menDianDao;

    @PostMapping("/del")
    public int del(@RequestBody Map map){
        return menDianDao.del(map);
    }

    @PostMapping("/addStudentData")
    public int addStudentData(@RequestBody Map map){
        return menDianDao.addStudentData(map);
    }

    @PostMapping("/dataStudent")
    public List<Map> dataStudent(@RequestBody Map map) throws InterruptedException {

        return menDianDao.dataStudent(map);
    }
}
