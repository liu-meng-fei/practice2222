package com.example.demo.controller;

import com.example.demo.dao.NoticeDao;
import com.example.demo.dao.WuLiaoLeiXingDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class WuLiaoLeiXingController {
    @Autowired
    WuLiaoLeiXingDao wuliaoleixingDao;

    @PostMapping("/del_wuliaoleixing")
    public int del_WuLiaoLeiXing(@RequestBody Map map){
        return wuliaoleixingDao.del_wuliaoleixing(map);
    }

    @PostMapping("/add_wuliaoleixing")
    public int add_WuLiaoLeiXing(@RequestBody Map map){
        return wuliaoleixingDao.add_wuliaoleixing(map);
    }

    @PostMapping("/find_all_wuliaoleixing")
    public List<Map> find_all_WuLiaoLeiXing(@RequestBody Map map) throws InterruptedException {

        return wuliaoleixingDao.find_all_wuliaoleixing(map);
    }
}
