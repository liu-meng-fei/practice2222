package com.example.demo.controller;

import com.example.demo.dao.WlcqDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class WlcqController {
    @Autowired
    WlcqDao wlcqDao;

    @PostMapping("/del_wlcq")
    public int del_wlcq(@RequestBody Map map){
        return wlcqDao.del_wlcq(map);
    }

    @PostMapping("/upd_wlcq")
    public int upd_wlcq(@RequestBody Map map){
        return wlcqDao.upd_wlcq(map);
    }

    @PostMapping("/add_wlcq")
    public int add_wlcq(@RequestBody Map map){
        return wlcqDao.add_wlcq(map);
    }

    @PostMapping("/find_all_wlcq")
    public List<Map> find_all_wlcq(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(2000);
        return wlcqDao.find_all_wlcq(map);
    }
    @PostMapping("/count_all_wlcq")
    public long count_all_wlcq(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(2000);
        return wlcqDao.count_all_wlcq(map);
    }
}