package com.example.demo.dao;
import java.util.List;
import java.util.Map;
public interface NoticeDao {
    List<Map> find_all_notice(Map map);
    long count_all_notice(Map map);
    int add_notice(Map map);
    int del_notice(Map map);
    int upd_notice(Map map);
}









