package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface WlCangkuDao {
    List<Map> find_all_wlcangku(Map map);
    long count_all_wlcangku(Map map);
    int add_wlcangku(Map map);
    int del_wlcangku(Map map);
    int upd_wlcangku(Map map);
}
