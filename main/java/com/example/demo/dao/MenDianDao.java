package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface MenDianDao {
    List<Map> dataStudent(Map map);
    long count_all_mendian(Map map);
    int addStudentData(Map map);
    int del(Map map);
    int update(Map map);
}