package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface baowaidanDao {
    List<Map> find_all_baowaidan(Map map);
    long count_all_baowaidan(Map map);
    int add_baowaidan(Map map);
    int del_baowaidan(Map map);
    int upd_baowaidan(Map map);
}
