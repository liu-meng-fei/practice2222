var datas = [];

loadData();

loadWuLiaoKu();

function loadWuLiaoKu(){
    loading.style.display = "block";
    ajax("/find_all_wuliaoku",{},function(wuliaokus){
        showData(wuliaokus);
        loading.style.display = "none";
    });
}

function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>物料类型</th><th>物料名称</th><th>物料编码</th><th>物料SN号</th>" +
        "<th>供应商类型</th><th>供应商名称</th><th>供应商编码</th>"+"<th>物料仓库</th><th>成本价</th><th>状态</th><th>操作</th></tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.wl_style+"</td>" +
            "<td class='ntitle'>"+a.wl_name+"</td>" +
            "<td class='ntitle'>"+a.wl_code+"</td>" +
            "<td class='ntitle'>"+a.wl_SN+"</td>" +
            "<td class='ntitle'>"+a.gys_style+"</td>" +
            "<td class='ntitle'>"+a.gys_name+"</td>" +
            "<td class='ntitle'>"+a.gys_code+"</td>" +
            "<td class='ntitle'>"+a.wlck+"</td>" +
            "<td class='ntitle'>"+a.daijia+"</td>"
            + "<td class='ntitle'>"+a.zhuangtai+"</td>" +
            "<td class='ntitle'><a class='edit_a' href='#'>编辑</a>" +
            "<a class='del_a' onclick='del(\""+a.wl_name+"\")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;


}

function search(){
    loadData({
        wl_style:stitle.value,
        wl_name:stitle.value,
        wl_code:stitle.value,
        wl_SN:stitle.value,
        gys_style:stitle.value,
        gys_name:stitle.value,
        gys_code:stitle.value,
        status:stitle.value,
    });
}

function loadData(data){
    list_div.innerHTML = "";
    loading.style.display = "block";
    ajax("/find_all_wuliaoku",data,function(data){
        datas = data;
        showData(datas);
         loading.style.display = "none";
    });
}


function returnMain(){
    list_div.style.display = "block";
    add_div.style.display = "none";
    search_div.style.display = "block";
    return_btn.style.display = "none";
    save_btn.style.display = "none";
    add_btn.style.display = "block";
}


function save(){
    if(!wl_name.value){
        wl_name_error.innerHTML = "请您填写物料名称";
      return;
    }
    ajax("/add_wuliaoku",{
        wl_style:wl_style.value,
        wl_name: wl_name.value,
        wl_code: wl_code.value,
        wl_SN: wl_SN.value,
        gys_style:gys_style.value,
        gys_name: gys_name.value,
        gys_code: gys_code.value,
        wlck:wlck.value,
        daijia:daijia.value,
        zhuangtai:zhuangtai.value,
        },function(data){
        alert("新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
    })

}

function del(wl_name){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    ajax("/del_wuliaoku", {wl_name:wl_name},function(data){
       alert("删除成功！");
       loadData();
    });
}


function returnMain(){
    list_div.style.display = "block";
    add_div.style.display = "none";

    return_btn.style.display = "none";
    save_btn.style.display = "none";
    add_btn.style.display = "block";
}

function add(){
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";

    return_btn.style.display = "block";
}