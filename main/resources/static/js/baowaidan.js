var baowaidans = [];
loadData();

function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>订单类型</th><th>子类型</th><th>厂家单号</th>" +
        "<th>状态</th><th>创建时间</th><th>修改时间</th><th>操作</th></tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.type+"</td>" +
            "<td class='ntitle'>"+a.type_name+"</td>" +
            "<td class='ntitle'>"+a.factory_num+"</td>" +
            "<td class='ntitle'>"+a.admin_status+"</td>" +
            "<td class='ntitle'>"+a.build_time+"</td>"+
            "<td class='ntitle'>"+a.revise_time+"</td>"+
            "<td class='ntitle'><a class='edit_a' href='#'>编辑</a>" +
            "<a class='del_a' onclick='del("+a.id+")' href='javascript:;'>删除</a></td>" +
    
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;
}

function search(){
    loadData({type:stitle.value,type_name:stitle.value,factory_num:stitle.value});
}

function loadData(data){
    list_div.innerHTML = "";
    loading.style.display = "block";
    var req = new XMLHttpRequest();
    req.open("post","/find_all_baowaidan");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    if(!data) data = {}
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            baowaidans = JSON.parse(req.responseText);
            showData(baowaidans);
            loading.style.display = "none";
        }
    }
}

function save(){
    var myDate=new Date()
    ajax("/add_baowaidan",{
        type:type.value,
        type_name:type_name.value,
        factory_num:factory_num.value,
        admin_status:admin_status.value,
        build_time:build_time.value,
        revise_time:revise_time.value
    },function(data){
        alert("新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
    })

}


function del(id){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    var req = new XMLHttpRequest();
    req.open("post","/del_baowaidan");
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    var data = {
        id:id
    }
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            if(req.responseText.trim()=="1"){
                alert("删除成功！");
            }

            loadData();
        }
    }
}

function add(){
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
}